package com.samasamastudios;

import com.samasamastudios.base.BaseGraphics;
import com.samasamastudios.base.BasePointF;
import com.samasamastudios.base.BasePolygon;
import lombok.Data;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by josemanuelprietogaray on 8/25/14.
 */
@Data
public class HexagonalGrid {

    private int size;
    private int mapSquareSize;
    private List<Hexagon> hexagons = new ArrayList<Hexagon>();
    private Hexagon[][] hexagonsMap;
    private float x;
    private float y;

    public static final int NEIGHBOR_NONE = -1;
    public static final int NEIGHBOR_RIGHT_BOTTOM = 0;
    public static final int NEIGHBOR_RIGHT_TOP = 1;
    public static final int NEIGHBOR_TOP = 2;
    public static final int NEIGHBOR_LEFT_TOP = 3;
    public static final int NEIGHBOR_LEFT_BOTTOM = 4;
    public static final int NEIGHBOR_BOTTOM = 5;

    private static int[][] NEIGHBORS = {
            {  1,  0 }, {  1, -1 }, {  0, -1 },
            { -1,  0 }, { -1,  1 }, {  0,  1 }
    };

    public static Color colorHexagonBackgroundOutline = Color.black; // new Color(0x8B4513);
    public static Color colorHexagonBackground = new Color(0x006600);
    public static Color colorHexagonHighlighted = Color.yellow;
    public static Color colorHexagonSelected = Color.green;
    public static Color colorHexagonInvalid = Color.red;
    public static Color colorHexagonSuggested = Color.blue;
    public static Color colorHexagonUsed = Color.gray;

    public HexagonalGrid(int size, float x, float y) {

        this.size = size;
        this.x = x;
        this.y = y;

        mapSquareSize = 3 + (2 * (size - 1));
        hexagonsMap = new Hexagon[mapSquareSize][mapSquareSize];

        for (int q = -size; q < (size + 1); q++) {
            for (int r = -size; r < (size + 1); r++) {
                if (Math.abs(q + r) <= size) {
                    Hexagon hexagon = new Hexagon(q, r, this, x, y);
                    hexagons.add(hexagon);
                    hexagonsMap[q + size][r + size] = hexagon;
                }
            }
        }

        for (Hexagon hexagon : hexagons) {
            //System.out.println("(" + hexagon.getQ() + ", " + hexagon.getR() + ")");
        }

        for (int row = 0; row < hexagonsMap.length; row++) {
            for (int column = 0; column < hexagonsMap[0].length; column++) {
                Hexagon hexagon = hexagonsMap[row][column];
                if (hexagon != null) {
                    //System.out.println("(" + hexagon.getQ() + ", " + hexagon.getR() + ")");
                }
            }
        }

//        Hexagon test = getHexagon(1, 1);
//        if (test != null) {
//            test.setSelected(true);
//        }
    }

    public Hexagon getHexagonByScreenCoordinates(int x, int y) {
        int q = Math.round((2f / 3f) * (x - this.x) / Hexagon.DEFAULT_SIZE);
        int r = (int) Math.round((((-1f / 3f) * (x - this.x)) + (((1f / 3f) * Math.sqrt(3f) * (y - this.y)))) / Hexagon.DEFAULT_SIZE);
        return getHexagon(q, r);
    }

    public Hexagon getHexagon(int q, int r) {

        //System.out.println("getHexagon(" + q + ", " + r + ")");

        if (Math.abs(q + r) > size) {
            //System.out.println("1) Not valid q, r = (" + q + ", " + r + ")");
            return null;
        }

        int transformedQ = (q + size);
        int transformedR = (r + size);
        if ((transformedQ >= mapSquareSize) || (transformedR >= mapSquareSize) || (transformedQ < 0) || (transformedR < 0)) {
            //System.out.println("2) Not valid q, r = (" + q + ", " + r + ")");
            return null;
        }

        return hexagonsMap[transformedQ][transformedR];
    }

    public Hexagon getHexagonByNeighborType(Hexagon hexagon, int neighborType) {
        int q = (int) (hexagon.getQ() + NEIGHBORS[neighborType][0]);
        int r = (int) (hexagon.getR() + NEIGHBORS[neighborType][1]);
        return getHexagon(q, r);
    }

    public List<Hexagon> getHexagonNeighbors(Hexagon hexagon) {
        List<Hexagon> neighbors = new ArrayList<Hexagon>();
        for (int i = 0; i < HexagonalGrid.NEIGHBORS.length; i++) {
            Hexagon neighbor = getHexagonByNeighborType(hexagon, i);
            if (neighbor != null) {
                neighbors.add(neighbor);
            }
        }
        return neighbors;
    }

    public static boolean areNeighbors(Hexagon hexagon1, Hexagon hexagon2) {
        return (HexagonalGrid.getNeighborType(hexagon1, hexagon2) != HexagonalGrid.NEIGHBOR_NONE);
    }

    public static int getNeighborType(Hexagon from, Hexagon to) {
        int offsetQ = Math.round(to.getQ() - from.getQ());
        int offsetR = Math.round(to.getR() - from.getR());
        int neighborTypeHexagon = HexagonalGrid.getNeighborType(offsetQ, offsetR);
        return neighborTypeHexagon;
    }

    public static int getNeighborType(int q, int r) {
        int neighborType = -1;
        int index = 0;
        while ((neighborType == -1) && (index < NEIGHBORS.length)) {
            if (q == NEIGHBORS[index][0] && r == NEIGHBORS[index][1]) {
                neighborType = index;
            }
            index++;
        }
        return neighborType;
    }

    public static BasePolygon getIntersectionPolygon(Hexagon hexagon1, Hexagon hexagon2) {
        if (!areNeighbors(hexagon1, hexagon2)) {
            return null;
        }

        BasePolygon intersectionPolygon = new BasePolygon();
        int neighborTypeHexagon1 = HexagonalGrid.getNeighborType(hexagon1, hexagon2);
        int neighborTypeHexagon2 = HexagonalGrid.getNeighborType(hexagon2, hexagon1);

        int sides = 6;
        float size = Hexagon.DEFAULT_SIZE;
        float angleStep = (float) ((2 * Math.PI) / sides);

        float point1Angle = (angleStep * (neighborTypeHexagon1 - 1));
        float point1X = hexagon1.getX() + (size - Hexagon.INNER_OFFSET) * (float) Math.cos(point1Angle);
        float point1Y = hexagon1.getY() - (size - Hexagon.INNER_OFFSET) * (float) Math.sin(point1Angle);
        BasePointF hexagon1Point1 = new BasePointF(point1X, point1Y);

        float point2Angle = (angleStep * (neighborTypeHexagon1));
        float point2X = hexagon1.getX() + (size - Hexagon.INNER_OFFSET) * (float) Math.cos(point2Angle);
        float point2Y = hexagon1.getY() - (size - Hexagon.INNER_OFFSET) * (float) Math.sin(point2Angle);
        BasePointF hexagon1Point2 = new BasePointF(point2X, point2Y);

        point1Angle = (angleStep * (neighborTypeHexagon2 - 1));
        point1X = hexagon2.getX() + (size - Hexagon.INNER_OFFSET) * (float) Math.cos(point1Angle);
        point1Y = hexagon2.getY() - (size - Hexagon.INNER_OFFSET) * (float) Math.sin(point1Angle);
        BasePointF hexagon2Point1 = new BasePointF(point1X, point1Y);

        point2Angle = (angleStep * (neighborTypeHexagon2));
        point2X = hexagon2.getX() + (size - Hexagon.INNER_OFFSET) * (float) Math.cos(point2Angle);
        point2Y = hexagon2.getY() - (size - Hexagon.INNER_OFFSET) * (float) Math.sin(point2Angle);
        BasePointF hexagon2Point2 = new BasePointF(point2X, point2Y);

        intersectionPolygon.getPoints().add(hexagon1Point1);
        intersectionPolygon.getPoints().add(hexagon1Point2);
        intersectionPolygon.getPoints().add(hexagon2Point1);
        intersectionPolygon.getPoints().add(hexagon2Point2);

        return intersectionPolygon;
    }

    public float getDistance(Hexagon hexagon1, Hexagon hexagon2) {
        return (Math.abs(hexagon1.getQ() - hexagon2.getQ()) + Math.abs(hexagon1.getR() - hexagon2.getR())
                + Math.abs(hexagon1.getQ() + hexagon1.getR() - hexagon2.getQ() - hexagon1.getR())) / 2;
    }

    public void draw(BaseGraphics g) {

        for (Hexagon hexagon : hexagons) {
            hexagon.draw(g);
        }
    }

    public void destroy() {
        for (int row = 0; row < hexagonsMap.length; row++) {
            for (int column = 0; column < hexagonsMap[0].length; column++) {
                Hexagon hexagon = hexagonsMap[row][column];
                if (hexagon != null) {
                    hexagon = null;
                }
            }
        }
        hexagonsMap = null;
        hexagons = null;
    }
}
