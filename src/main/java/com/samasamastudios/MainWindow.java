package com.samasamastudios;

import javax.swing.*;

public class MainWindow extends JFrame {

    public static int SCREEN_WIDTH = 1000;
    public static int SCREEN_HEIGHT = 800;

    public static void main(String[] args) {
        MainWindow main = new MainWindow();
        main.add(new DrawingPanel());
        main.setVisible(true);
    }

    public MainWindow() {
        setTitle("Hexagonal Life");
        setSize(MainWindow.SCREEN_WIDTH, MainWindow.SCREEN_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}