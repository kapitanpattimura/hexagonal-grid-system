package com.samasamastudios;

import com.samasamastudios.base.BaseGraphics;
import com.samasamastudios.base.BasePointF;
import com.samasamastudios.base.BasePolygon;
import lombok.Data;

import java.awt.Color;

/**
 * Created by josemanuelprietogaray on 8/25/14.
 */
@Data
public class Hexagon {

    public static final boolean DEBUG_COORDINATES = false;

    private float x;
    private float y;

    public float q;
    public float r;

    private float size;
    private BasePolygon polygon;
    private BasePolygon highlightPolygon;
    public static final float DEFAULT_SIZE = 50f;

    public static float INNER_OFFSET = 3;

    private boolean selected = false;
    private boolean highlighted = false;
    private boolean invalid = false;
    private boolean suggested = false;
    private boolean used = false;
    private boolean visible = true;
    private boolean seen = false;

    private Object tag;

    public Hexagon(float q, float r, HexagonalGrid grid, float gridCenterX, float gridCenterY) {

        this.q = q;
        this.r = r;

        size = Hexagon.DEFAULT_SIZE;
        polygon = new BasePolygon();
        highlightPolygon = new BasePolygon();

        this.x = gridCenterX + size * 3/2 * q;
        this.y = gridCenterY + size * (float) Math.sqrt(3) * (r + q / 2);

        //System.out.println("(x=" + x + ",y=" + y + ")");

        int sides = 6;
        for (int i = 0; i < sides; i++) {
            float angle = (float) ((2 * Math.PI) / sides) * (i + 1);
            float pointX = x + (size - INNER_OFFSET) * (float) Math.cos(angle);
            float pointY = y + (size - INNER_OFFSET) * (float) Math.sin(angle);
            polygon.getPoints().add(new BasePointF(pointX, pointY));
        }

        sides = 6;
        for (int i = 0; i < sides; i++) {
            float angle = (float) ((2 * Math.PI) / sides) * (i + 1);
            float pointX = x + (size) * (float) Math.cos(angle);
            float pointY = y + (size) * (float) Math.sin(angle);
            highlightPolygon.getPoints().add(new BasePointF(pointX, pointY));
        }
    }

    public void draw(BaseGraphics g) {
//        if (!seen) {
//            return;
//        }

        if (highlighted) {
            g.setColor(HexagonalGrid.colorHexagonHighlighted);
        } else if (invalid) {
            g.setColor(HexagonalGrid.colorHexagonInvalid);
        } else if (suggested) {
            g.setColor(HexagonalGrid.colorHexagonSuggested);
        } else if (used) {
            g.setColor(HexagonalGrid.colorHexagonUsed);
        } else {
            g.setColor(HexagonalGrid.colorHexagonBackgroundOutline);
        }
        g.fillPolygon(highlightPolygon);

        if (selected) {
            g.setColor(HexagonalGrid.colorHexagonSelected);
            g.fillPolygon(highlightPolygon);
        }
        g.setColor(HexagonalGrid.colorHexagonBackground);
        g.fillPolygon(polygon);

        if (DEBUG_COORDINATES) {
            g.drawString("(" + q + ", " + r + ")", x - 25, y);
        }
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        if (visible) {
            this.setSeen(true);
        }
    }

    public float getWidth() {
        return (size * 2);
    }

    public float getHeight() {
        return ((float) (Math.sqrt(3) / 2) * getWidth());
    }
}
