package com.samasamastudios.base;

import lombok.Data;

/**
 * Created by josemanuelprietogaray on 8/28/14.
 */
@Data
public class BaseRectangle {
    private float left;
    private float top;
    private float bottom;
    private float right;

    public BaseRectangle(float left, float top, float right, float bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public boolean contains(float x, float y) {
        return ((x >= this.left && x <= this.right) && (y >= this.top && y <= this.bottom));
    }
}
