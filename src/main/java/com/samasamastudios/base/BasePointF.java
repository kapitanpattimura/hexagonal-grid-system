package com.samasamastudios.base;

import lombok.Data;

/**
 * Created by josemanuelprietogaray on 8/25/14.
 */
@Data
public class BasePointF {
    private float x;
    private float y;

    public BasePointF(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
