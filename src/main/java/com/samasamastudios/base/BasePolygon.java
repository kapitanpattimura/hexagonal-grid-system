package com.samasamastudios.base;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by josemanuelprietogaray on 8/25/14.
 */
@Data
public class BasePolygon {
    List<BasePointF> points = new ArrayList<BasePointF>();
}
