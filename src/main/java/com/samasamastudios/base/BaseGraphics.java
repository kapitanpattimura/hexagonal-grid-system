package com.samasamastudios.base;

import java.awt.*;

/**
 * Created by josemanuelprietogaray on 8/25/14.
 */
public class BaseGraphics {

    private Graphics graphics;

    public static final int HCENTER = 1;
    public static final int VCENTER = 1 << 1;
    public static final int LEFT = 1 << 2;
    public static final int RIGHT = 1 << 3;
    public static final int TOP = 1 << 4;
    public static final int BOTTOM = 1 << 5;

    private Color outlineColor = Color.black;

    public BaseGraphics(Graphics graphics) {
        this.graphics = graphics;
    }

    private void drawPolygon(BasePolygon polygon, boolean filled) {
        Polygon p = new Polygon();
        for (int i = 0; i < polygon.getPoints().size(); i++) {
            BasePointF pointF = polygon.getPoints().get(i);
            p.addPoint((int) pointF.getX(), (int) pointF.getY());
        }
        if (filled) {
            graphics.fillPolygon(p);
        } else {
            graphics.drawPolygon(p);
        }
    }

    public void drawRectangle(BaseRectangle rectangle) {
        graphics.drawRect(Math.round(rectangle.getLeft()),
                Math.round(rectangle.getTop()),
                Math.round(rectangle.getRight() - rectangle.getLeft()),
                Math.round(rectangle.getBottom() - rectangle.getTop()));
    }

    public void fillRectangle(BaseRectangle rectangle) {
        graphics.fillRect(Math.round(rectangle.getLeft()),
                Math.round(rectangle.getTop()),
                Math.round(rectangle.getRight() - rectangle.getLeft()),
                Math.round(rectangle.getBottom() - rectangle.getTop()));
    }

    public void drawLine(float left, float top, float right, float bottom ) {
        graphics.drawLine((int) left, (int) top, (int) right, (int) bottom);
    }

    public void drawPolygon(BasePolygon polygon) {
        drawPolygon(polygon, false);
    }

    public void fillPolygon(BasePolygon polygon) {
        drawPolygon(polygon, true);
    }

    public void drawStringWithOutline(String text, float x, float y, int align) {
        Color currentColor = graphics.getColor();
        graphics.setColor(outlineColor);
        int offset = 1;
        drawString(text, x, y, align);
        drawString(text, x + offset, y, align);
        drawString(text, x + offset, y, align);
        drawString(text, x + offset, y + offset, align);
        drawString(text, x, y + offset, align);
        drawString(text, x - offset, y + offset, align);
        drawString(text, x - offset, y, align);
        drawString(text, x - offset, y - offset, align);
        drawString(text, x, y - offset, align);
        graphics.setColor(currentColor);
        drawString(text, x, y, align);
    }

    public void drawString(String text, float x, float y, int align) {
        if ((align & BaseGraphics.VCENTER) != 0) {
            y += graphics.getFontMetrics().getHeight() / 2;
        } else if ((align & BaseGraphics.VCENTER) != 0) {
            y -= (graphics.getFontMetrics().getHeight());
        }
        for (String line : text.split("\n")) {
            if ((align & BaseGraphics.HCENTER) != 0) {
                double width = graphics.getFontMetrics().getStringBounds(line, graphics).getWidth();
                float newX = (float) (x - (width / 2));
                graphics.drawString(line, (int) newX, (int) y);
            } else {
                graphics.drawString(line, (int) x, (int) y);
            }
            y += graphics.getFontMetrics().getHeight();
        }
    }

    public int setFontSize(int fontSize) {
        int currentFontSize = graphics.getFont().getSize();
        Font font = new Font(graphics.getFont().getFontName(), graphics.getFont().getStyle(), fontSize);
        graphics.setFont(font);
        return currentFontSize;
    }

    public void drawString(String text, float x, float y) {
        drawString(text, x, y, BaseGraphics.LEFT | BaseGraphics.TOP);
    }

    public void setColor(Color color) {
        graphics.setColor(color);
    }

    public void setOutlineColor(Color color) {
        outlineColor = color;
    }
}
