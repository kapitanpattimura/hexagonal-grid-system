package com.samasamastudios;

import com.samasamastudios.base.BaseGraphics;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;

/**
 * Created by josemanuelprietogaray on 8/25/14.
 */
public class DrawingPanel extends JPanel implements MouseListener, MouseMotionListener {

    BaseGraphics baseGraphics;
    HexagonalGrid grid;
    Hexagon selectedHexagon = null;

    public DrawingPanel() {
        setBorder(BorderFactory.createLineBorder(Color.black));
        addMouseListener(this);
        addMouseMotionListener(this);

        createGrid();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        baseGraphics = new BaseGraphics(g);
        grid.draw(baseGraphics);
    }

    public void createGrid() {
        if (grid != null) {
            grid.destroy();
        }
        grid = new HexagonalGrid(10, MainWindow.SCREEN_WIDTH / 2, MainWindow.SCREEN_HEIGHT / 2);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK) {
            createGrid();
        } else {
            Hexagon hexagon = grid.getHexagonByScreenCoordinates(e.getX(), e.getY());
            if (hexagon != null) {
                hexagon.setSelected(true);
                List<Hexagon> neighbors = grid.getHexagonNeighbors(hexagon);
                if (neighbors != null) {
                    for (int i = 0; i < neighbors.size(); i++) {
                        neighbors.get(i).setSelected(true);
                    }
                }
            }
        }
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

        Hexagon hexagon = grid.getHexagonByScreenCoordinates(e.getX(), e.getY());

        if (hexagon != null) {
            if (selectedHexagon == null) {
                selectedHexagon = hexagon;
                selectedHexagon.setHighlighted(true);
            }
            if (hexagon != selectedHexagon) {
                selectedHexagon.setHighlighted(false);
                selectedHexagon = hexagon;
                selectedHexagon.setHighlighted(true);
            }
        }

        repaint();
    }
}
